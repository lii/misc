/*

Implementation of the Micro Kanren algorithm in Scala.

This turned out to be pretty nice. It's interesting that I could do an almost identical translation from Haskell.

My thought about the work:

* Laziness turned out to be the biggest difference from Haskell. It didn't take a big change to the code to make it lazy but it's tricky to understand exactly how to do it and how it works now. And it's not lazy in the first element right now, how could that be achieved in a nice way?
* Which kinds of functions can easily be made lazy in this way?
* I like the idiom where you pattern match on the type of an object and then get data from it by its fields. This is nicer than using record field accessors in Haskell.
* The Scala Eclipse plug-in has some features that would be really useful for Haskell also: The ability to display the inferred type of an expression. A command for inserting a type annotation at for an expression.
 
*/

package testing

import scala.language.implicitConversions

case class Id(id: String)

case class VarId(value: Int) {
   def next: VarId = VarId(value + 1)
   override def toString = value.toString
}

sealed trait Goal

case class Unify(term1: Term, term2: Term) extends Goal
case class Conj(goal1: Goal, goal2: Goal)  extends Goal
case class Disj(goal1: Goal, goal2: Goal)  extends Goal
case class Let(id: Id, body: Goal) extends Goal

sealed trait Term

case class Var(id: Id) extends Term
case class Value(value: Int) extends Term
case class Pair(a: Term, b: Term) extends Term

case class State(
  val substs : Map[VarId, Term]  = Map(),
  val varMap : Map [Id, VarId] = Map(),
  val nextVar: VarId = VarId(0))

object Kanren {
  
  def eval(goal: Goal, state: State): Stream[State] = evalInner(goal, state)    
  
  def evalInner(goal: Goal, state: State): Stream[State] = {
    // println("Goal: " + goal)
    goal match {
      case disj: Disj => eval(disj.goal1, state) #::: eval(disj.goal2, state)
      case conj: Conj => eval(conj.goal1, state).flatMap(newState => eval(conj.goal2, newState))
      case let: Let   => eval(let.body, 
        State(
          state.substs, // + (state.nextVar -> Var(l.id)),
          state.varMap + (let.id -> state.nextVar),
          state.nextVar.next))
      case unif: Unify => 
        unify(unif.term1, unif.term2, state.substs, state.varMap) match {
          case Some(newSubsts) => Stream(State(newSubsts, state.varMap, state.nextVar))
          case None => Stream()
        }
    }
  }
  
  def unify(t1 : Term, t2 : Term, substs : Map[VarId, Term], varMap : Map[Id, VarId]) : Option[Map[VarId, Term]] = { 
      val term1 = resolve(t1, substs, varMap)
      val term2 = resolve(t2, substs, varMap)
      
      (term1, term2) match {
        case (v1: Var, v2: Var) if v1 == v2 => Some(substs)
        case (v1: Var, _) => Some(substs + (varMap(v1.id) -> term2))
        case (_, v2: Var) => Some(substs + (varMap(v2.id) -> term1))
        case (v1: Value, v2: Value) => if (v1 == v2) Some(substs) else None
        case (p1: Pair, p2: Pair) =>
          unify(p1.a, p2.a, substs, varMap) match {
            case Some(newSubsts) => unify(p1.b, p2.b, newSubsts, varMap)
            case None => None
          }
        case _ => None
      }
  }

  def resolve(t: Term, substs: Map[VarId, Term], varMap: Map[Id, VarId]): Term = {
    t match {
      case v: Var => 
        substs.get(varMap(v.id)) match {
          case Some(r) => resolve(r, substs, varMap)
          case None => v
        }
      case _ => t
    }
  }
  
  implicit def stringToId(s: String): Id = Id(s)
  implicit def intToVarId(i: Int): VarId = VarId(i)
  implicit def stringToVar(s: String): Var = Var(s)
  implicit def intToValue(i: Int): Value = Value(i)
  
  def main(args: Array[String]) {
    
//    test1
//    test2
//    test3
    test4
    
    println("\nAssertions passed.")
  }
  
  def test4 = {
    val g = Let("a", Let("b", Let("c", 
      Disj(
        Disj(Unify("a", 1), Disj(Unify("b", 2), Unify("c", 3))),
        Disj(Unify("a", 4), Disj(Unify("b", 5), Unify("c", 6)))))))
          
    val r = eval(g, State())
    println("eval called")

    println("head: " + r.head)
    
    val t = r.tail
    
    println("tail head: " + t.head)

    
    println("Result 4: " + r.toList)
  }

  def test3 = {
    val g3 = Let("a", Let("b", Let("c", 
        Unify(
          Pair("a", Pair("c", 2)), 
          Pair("c", Pair("a", "b"))))))
    
    val r3 = eval(g3, State()).toList

    println("Result 3: " + r3)
      
    assert(r3 == 
          List(State(Map((0, Var(Id("c"))), (1, Value(2))),Map((Id("a"), 0), (Id("b"), 1), (Id("c"), 2)),3)))
  }

  def test2 = {
    val g2 = Let("a", Let("b", Let("c", 
        Unify(
          Pair("a", Pair("c", 2)), 
          Pair(1, Pair("a", "b"))))))

    val r2 = eval(g2, State()).toList
    
    println("Result 2: " + r2)
    assert(r2 ==
      List(State(
        Map((0, Value(1)), (2, Value(1)), (1, Value(2))),
        Map((Id("a"), VarId(0)), (Id("b"), VarId(1)), (Id("c"), VarId(2))), 
        3)))
  }

  def test1 = {
    val g1 = Let("v", Unify(Pair("v", 2), Pair(1, 2)))
    val r1 = eval(g1, State()).toList
    println("Result 1: " + r1)
    assert(r1 == List(State(Map((0, Value(1))), Map(("v", 0)), 1)))
  }
}


