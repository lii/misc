This post ended up being an entire blog post about a `Result` monad in Java. Since I don't have a blog I just post it here anyway.

---

Operations which either compute a result or fail are very common. They always lead to the question about what kind of object to return from such functions. Value-or-null has no type safety, `Optional` (aka `Maybe`) doesn't let you return diagnostic messages, exceptions unnecessarily drags in non-local jumps, `Alternative` (aka `Either`) doesn't communicate the intend very clearly. I myself often create a simple a simple `Result` class:

    class Result<T> {
        T restul; // Contains the result if operation succeeded
        List<String> errorMessages; // Contains error messages if operation failed
    }

Pretty convenient.

But a problem is that if I want to perform operation one after the other I end up writing awkward code with a lot of nesting, like this:

    Result<Integer> r1 = someOperation();
    
    if (r1.isSuccess()) {
        Result<Double> r2 = someOtherOperation(r1.result);
        if (r2.isSuccess()) {
            return r2.result;
        } else {
            System.out.println(r2.errorMessages);
        }
    } else {
        System.out.println(r1.errorMessages);
    }

    return 0.0;

The last time that happened however I added a new method to the `Result` class:

    public <R> Result<R> continueIfSuccess(Function<T, Result<R>> nextOperation) {
        if (isSuccess()) return nextOperation.apply(result);
        else return new Result<>(null, errorMessages);
    }

I have now captured the one-after-the-other pattern as a method and the code looks much prettier:

    Result<Double> r = someOperation()
        .continueIfSuccess(i -> someOtherOperation(i));
    
    if (r.isSuccess()) {
        return r.result;
    } else {
        System.out.println(r.errorMessages);
        return 0.0;
    }

But wait a minute! Don't I recognise the type of `continueIfSuccess` from somewhere? Let's write it in the conventional function type format, including the *this* arguemnt:

    continueIfSuccess :: Result<T> -> (T -> Result<R>>) -> Result<R>

And let's compare that with a [standard library function][1] of a popular functional programming language:

    (>>=) :: m a -> (a -> m b) -> m b

It turns about that `continueIfSuccess` has the same type as the monad *bind* operation, which is written `>>=` above. This means that I have by accident turned `Result` into a monad.

---

**PS**

I also added the following operation to `Result`:

    public Result<T> tryIfFailure(Supplier<Result<T>> nextOperation) { ... }

It essentially have to following type (since the `Supplier` argument is just a way to express a lazy value):

    tryIfFailure :: Result<T> -> Result<T> -> Result<T>

I don't know if this corresponds to any monad operation.

[1]: https://hackage.haskell.org/package/base-4.9.0.0/docs/Control-Monad.html#v:-62--62--61-    
