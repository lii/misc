Basic support for new languages can be added easily using standard Eclipse features and some custom plug-ins.

* **Syntax colors** can be added with either the [Eclipse Colorer][colorer_page] or [LiClipse][liclipse_page].
They are plug-ins that provides syntax colors for a large number of languages. New languages can be added easily using custom color files.

* To **build** using an external compiler from inside Eclipse the *Program Builder* feature can be used. The builder is run when the normal *Build Project* command is issued. (To set up: *Project Properties -> Builders -> New... -> Program*)

* To **capture compile errors and warning** from an external builder a build output parser can be added with the [Laid Builder Marker][build_marker_page] plug-in. This output parser puts problem markers in editors and in the Problems view.

* To **run** compiled programs from inside Eclipse the use the *External Tools* feature. (*Run* -> *External Tools*)

* Extended support for **code navigation and text selection** can be added with some of the [other][marker_block_page] Laid project [plug-ins][editor_utils_page].

* Use the *Word Completion* command (*Shift+Alt+7*) as a poor persons's content assist.

* Use the *Toggle Block Selection* command (Shift+Alt+A) to insert/remove line comments on multiple lines at the same time.

* Some other useful standard Eclipse features include: the *Open Resource* command (*Ctrl+Shift+R*); the *File Search* command (*Ctrl+H*); and the bookmarks feature (*Edit -> Add bookmark*). Make sure to check *Include in next/prev navigation* box (*Preferences -> General -> Editors -> Text Editors -> Annotations -> Bookmarks*).

I have created a [document][set_up_doc] that describes in more detail how simple language support can be added to Eclipse with minimal effort. It describes how to set up and configure the plug-ins mentioned above and a few other things.

Disclaimer: I am the author of the [Laid Language Tools][laid_page] project.

[laid_page]: https://bitbucket.org/lii/laid_language_tools/
[set_up_doc]: https://bitbucket.org/lii/laid_language_tools/src/master/Setting_up_Eclipse_for_new_language.md
[colorer_page]: http://colorer.sourceforge.net/eclipsecolorer/
[build_marker_page]: https://bitbucket.org/lii/laid_language_tools/src/HEAD/se.lidestrom.laid.build_marker
[editor_utils_page]: https://bitbucket.org/lii/laid_language_tools/src/HEAD/se.lidestrom.laid.editor_utils
[marker_block_page]: https://bitbucket.org/lii/laid_language_tools/src/HEAD/se.lidestrom.laid.marker_block_selection
[liclipse_page]: http://www.liclipse.com/text
