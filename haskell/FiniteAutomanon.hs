{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}

module Main where

import qualified Data.Map as M
import qualified Data.Set as S

main :: IO ()
main = do
  putStrLn "Privjet mir!!!"

data Rule s c where
  Rule :: (Show s, Show c, Ord s, Ord c) => {
    rState ::  s,
    rSymbol :: c,
    rNextState :: s
  } -> Rule s c

deriving instance Eq   (Rule s c)
deriving instance Ord  (Rule s c)
deriving instance Show (Rule s c)


data RuleBook s c where
  RuleBook :: (Show s, Show c, Ord s, Ord c) => {
    bRules :: M.Map s (M.Map c (Rule s c))
  } -> RuleBook s c


deriving instance Eq   (RuleBook s c)
deriving instance Ord  (RuleBook s c)
deriving instance Show (RuleBook s c)

gotoNextState :: RuleBook s c -> s -> c -> s
gotoNextState RuleBook{ bRules = rs } state sym =
  rNextState ((rs M.! state) M.! sym)

data T a where
  T :: Show a => {
    f :: a
  } -> T a


data Dfsa s c = Dfsa{
    currentState :: s,
    acceptStates :: S.Set s,
    ruleBook     :: RuleBook s c
  }
  deriving (Eq, Ord, Show)

