{-

uKanren implementation where the type of the Kanren values have been moved out of the algorithm, into a type class that clients provide. The value type class provide a function that for two values returns if they might unify, and a list of sub-values that also need should unify. 

-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Map (Map)
import qualified Data.Map as M
import Data.String
import Language.Haskell.Exts.Parser
import Language.Haskell.Exts.Pretty

import Control.Monad (foldM)

newtype Id = Id String
  deriving (Eq, Ord, Show)

data Goal a =
  Unify (Term a) (Term a) |
  Conj (Goal a) (Goal a) |
  Disj (Goal a) (Goal a) | 
  Let Id (Goal a)
  deriving Show

data Term a =
  Var Id |
  Value a |
  Pair (Term a) (Term a)
  deriving Show

data State a = State{
    stSubsts  :: Map Int (Term a), -- Var index to var value. No mapping if var is unbound 
    stVarMap  :: Map Id Int,   -- Var name to var index. 
    stNextVar :: Int 
  }
  deriving Show


class IsValue v where
  unifyValues :: v -> v -> Maybe [(Term v, Term v)]


emptyState :: Eq a => State a
emptyState = State{
    stSubsts  = M.empty,
    stVarMap  = M.empty,
    stNextVar = 0
  }

-- Everything is parameterised on a, the type of values
eval :: IsValue a => Goal a -> State a -> [State a]
eval goal state@State{ stSubsts = subs, stVarMap = varMap, stNextVar = nextVar } = 
  case goal of
    Disj g1 g2  -> concat [eval g1 state, eval g2 state]
    Conj g1 g2  -> concat (map (\s -> eval g1 s) (eval g2 state))
    Let  i  g   -> eval g state{
          stVarMap = M.insert i nextVar varMap,
          stNextVar = nextVar + 1
        }
    Unify g1 g2 -> case unify subs varMap g1 g2 of
      Just newSubsts -> [state{ stSubsts = newSubsts }]
      Nothing        -> []


unify :: IsValue a => Map Int (Term a) -> Map Id Int -> Term a -> Term a -> Maybe (Map Int (Term a))
unify subs varMap t1 t2 =
  let
    term1 = resolve t1 subs varMap
    term2 = resolve t2 subs varMap
  in
    case (term1, term2) of
      (Var i1, Var i2) | i1 == i2 -> Just subs
      (Var i1, _)                 -> Just (M.insert (lookupVar i1 varMap) term2 subs)
      (_, Var i2)                 -> Just (M.insert (lookupVar i2 varMap) term1 subs)
      (Value v1, Value v2)        -> do
        -- Weird solution using Maybe monad:
        subTerms <- unifyValues v1 v2
        foldM 
          (\subTermSubs (subTerm1, subTerm2)  -> unify subTermSubs varMap subTerm1 subTerm2)
          subs subTerms
          -- Normal solution using case-expressions:      
--        -- Call user defined unify function
--        case unifyValues v1 v2 of
--          Nothing       -> Nothing
--          -- Fold over sub-terms of the value
--          Just subTerms -> foldr
--            (\(subTerm1, subTerm2) subTermSubs -> 
--              case subTermSubs of
--                Nothing -> Nothing
--                Just s  -> unify s varMap subTerm1 subTerm2) 
--            (Just subs)
--            subTerms
      _ -> Nothing


resolve :: Term a -> Map Int (Term a) -> Map Id Int -> Term a
resolve term subs varMap =
  case term of
    var@(Var i) ->
      case M.lookup (lookupVar i varMap) subs of
        Just result -> resolve result subs varMap
        Nothing     -> var
    _ -> term

------------------------------------------------------

instance IsString (Term a) where
  fromString = Var . Id

instance IsString Id where
  fromString = Id

lookupVar :: Id -> Map Id Int -> Int
lookupVar i varMap =
  case M.lookup i varMap of
    Just t -> t
    Nothing -> error ("Id " ++ show i ++ " is not in var map " ++ show varMap)

groom :: Show a => a -> String
-- Replace with this if you don't want to install haskell-src-exts:
-- groom = show
groom s = case parseExp (show s) of
    ParseOk x -> prettyPrintStyleMode
      Style{ mode = PageMode, lineLength = 150, ribbonsPerLine = 2.5 } defaultMode x
    ParseFailed{} -> show s

--------------------------------------------------


data NewValue =
  I Int |
  P (Term NewValue) (Term NewValue)


instance IsValue NewValue where
  unifyValues (I i1) (I i2) = if i1 == i2 then Just [] else Nothing
  unifyValues (P a1 a2) (P b1 b2) = Just [(a1, b1), (a2, b2)]
  unifyValues _ _ = Nothing

instance IsValue Int where
  unifyValues i1 i2 = if i1 == i2 then Just [] else Nothing

tryingResult :: [State Int]
tryingResult = tryingResult3

-- Basic unify
tryingResult0 :: [State Int]
tryingResult0 = eval
  (Let "v" (Unify "v" (Value 7)))
  emptyState

-- Output of expression above:
-- [State{stSubsts = fromList [(0, Value 7)], stNextVar = 1,
--        stVarMap = fromList [(Id "v", 0)]}]

-- Disjuction and conjunction
tryingResult1 :: [State Int]
tryingResult1 = eval
  (Conj
    (Let "a" (Unify "a" (Value 7)))
    (Let "b" (Disj
      (Unify "b" (Value 5))
      (Unify "b" (Value 6)))))
  emptyState

-- Output of expression above:
-- [State{stSubsts = fromList [(0, Value 5), (1, Value 7)],
--        stNextVar = 2,
--        stVarMap = fromList [(Id "a", 1), (Id "b", 0)]},
--  State{stSubsts = fromList [(0, Value 6), (1, Value 7)],
--        stNextVar = 2,
--        stVarMap = fromList [(Id "a", 1), (Id "b", 0)]}]

-- Unify with a var
tryingResult2 :: [State Int]
tryingResult2 = eval
  (Let "a" 
    (Conj
      (Unify "a" (Value 7))
      (Let "b" (Disj
        (Unify "b" (Var "a"))
        (Unify "b" (Value 6))))))
  emptyState

-- Output of expression above:
-- [State{stSubsts =
--          fromList [(0, Value 7), (1, Var (Id "a"))],
--        stNextVar = 2,
--        stVarMap = fromList [(Id "a", 0), (Id "b", 1)]},
--  State{stSubsts = fromList [(0, Value 7), (1, Value 6)],
--        stNextVar = 2,
--        stVarMap = fromList [(Id "a", 0), (Id "b", 1)]}]

-- Unify with a var, transitive
tryingResult3 :: [State Int]
tryingResult3 = eval
  (Let "a" 
    (Let "b" 
      (Let "c" 
        (Conj 
          (Unify "a" (Var "b"))
          (Conj 
            (Unify "b" "c")
            (Unify "a" (Value 7)))))))
  emptyState

-- Output of expression above:
-- [State{stSubsts =
--          fromList [(0, Value 7), (1, Var (Id "c")), (2, Value 7)],
--        stNextVar = 3,
--        stVarMap =
--          fromList [(Id "a", 0), (Id "b", 1), (Id "c", 2)]}]

main :: IO ()
main = putStrLn (groom tryingResult)

