{-

A uKanren implementation in Haskell. I think it turn out *much* nicer than the Lisp implementation from the paper!

The code size is similar to the one in the paper (if excluding data type declarations). And I really think the types makes it much easier to understand how it works. It's very valuable to be able to quickly see, for example,  exactly what a substitution map contains.

The solution is a little different from the one in the paper:

* I use deep embedding of the Kanren language (that is, the language components are simple data type, instead of functions and I use a common evaluation function) in contrast to the version in the paper, because I find deep embedding easier to understand. 
* There is some extra code complexity because I explicitly manage variable scope, instead of relying on lambdas in the host language. 
* I gave things names which I find more sensible, for example "Let" and "resolve" instead of "call/fresh" and "walk".

https://bitbucket.org/lii/misc/src/master/haskell/Kanren.hs?fileviewer=file-view-default

-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Map (Map)
import qualified Data.Map as M
import Data.String
import Language.Haskell.Exts.Parser
import Language.Haskell.Exts.Pretty

newtype Id = Id String
  deriving (Eq, Ord, Show)

data Goal =
  Unify Term Term |
  Conj Goal Goal |
  Disj Goal Goal | 
  Let Id Goal
  deriving Show

data Term =
  Var Id |
  Value Int |
  Pair Term Term
  deriving Show

data State = State{
    stSubsts  :: Map Int Term, -- Var index to var value. No mapping if var is unbound 
    stVarMap  :: Map Id Int,   -- Var name to var index. 
    stNextVar :: Int 
  }
  deriving Show

emptyState :: State
emptyState = State{
    stSubsts  = M.empty,
    stVarMap  = M.empty,
    stNextVar = 0
  }

eval :: Goal -> State -> [State]
eval goal state@State{ stSubsts = subs, stVarMap = varMap, stNextVar = nextVar } = 
  case goal of
    Disj g1 g2  -> concat [eval g1 state, eval g2 state]
    Conj g1 g2  -> concat (map (\s -> eval g1 s) (eval g2 state))
    Let  i  g   -> eval g state{
          stVarMap = M.insert i nextVar varMap,
          stNextVar = nextVar + 1
        }
    Unify g1 g2 -> case unify subs varMap g1 g2 of
      Just newSubsts -> [state{ stSubsts = newSubsts }]
      Nothing        -> []

unify :: Map Int Term -> Map Id Int -> Term -> Term -> Maybe (Map Int Term)
unify subs varMap t1 t2 =
  let
    term1 = resolve t1 subs varMap
    term2 = resolve t2 subs varMap
  in
    case (term1, term2) of
      (Var i1, Var i2) | i1 == i2 -> Just subs
      (Var i1, _)                 -> Just (M.insert (lookupVar i1 varMap) term2 subs)
      (_, Var i2)                 -> Just (M.insert (lookupVar i2 varMap) term1 subs)
      (Value v1, Value v2)        -> if v1 == v2 then Just subs else Nothing
      (Pair a1 a2, Pair b1 b2)    -> 
        case unify subs varMap a1 b1 of
          Just newSubs -> unify newSubs varMap a2 b2
          Nothing      -> Nothing
      _ -> Nothing

resolve :: Term -> Map Int Term -> Map Id Int -> Term
resolve var@(Var i) subs varMap =
  case M.lookup (lookupVar i varMap) subs of
    Just result -> resolve result subs varMap
    Nothing     -> var

resolve term _subs _varMap = term

------------------------------------------------------

instance IsString Term where
  fromString = Var . Id

instance IsString Id where
  fromString = Id

lookupVar :: Id -> Map Id Int -> Int
lookupVar i varMap =
  case M.lookup i varMap of
    Just t -> t
    Nothing -> error ("Id " ++ show i ++ " is not in var map " ++ show varMap)

groom :: Show a => a -> String
-- Replace with this if you don't want to install haskell-src-exts:
-- groom = show
groom s = case parseExp (show s) of
    ParseOk x -> prettyPrintStyleMode
      Style{ mode = PageMode, lineLength = 150, ribbonsPerLine = 2.5 } defaultMode x
    ParseFailed{} -> show s

--------------------------------------------------


tryingResult :: [State]
tryingResult = tryingResult3

-- Basic unify
tryingResult0 :: [State]
tryingResult0 = eval
  (Let "v" (Unify "v" (Value 7)))
  emptyState

-- [State{stSubsts = fromList [(0, Value 7)], stNextVar = 1,
--        stVarMap = fromList [(Id "v", 0)]}]

-- Disjuction and conjunction
tryingResult1 :: [State]
tryingResult1 = eval
  (Conj
    (Let "a" (Unify "a" (Value 7)))
    (Let "b" (Disj
      (Unify "b" (Value 5))
      (Unify "b" (Value 6)))))
  emptyState

-- [State{stSubsts = fromList [(0, Value 5), (1, Value 7)],
--        stNextVar = 2,
--        stVarMap = fromList [(Id "a", 1), (Id "b", 0)]},
--  State{stSubsts = fromList [(0, Value 6), (1, Value 7)],
--        stNextVar = 2,
--        stVarMap = fromList [(Id "a", 1), (Id "b", 0)]}]

-- Unify with a var
tryingResult2 :: [State]
tryingResult2 = eval
  (Let "a" 
    (Conj
      (Unify "a" (Value 7))
      (Let "b" (Disj
        (Unify "b" (Var "a"))
        (Unify "b" (Value 6))))))
  emptyState

-- [State{stSubsts =
--          fromList [(0, Value 7), (1, Var (Id "a"))],
--        stNextVar = 2,
--        stVarMap = fromList [(Id "a", 0), (Id "b", 1)]},
--  State{stSubsts = fromList [(0, Value 7), (1, Value 6)],
--        stNextVar = 2,
--        stVarMap = fromList [(Id "a", 0), (Id "b", 1)]}]

-- Unify with a var, transitive
tryingResult3 :: [State]
tryingResult3 = eval
  (Let "a" 
    (Let "b" 
      (Let "c" 
        (Conj 
          (Unify "a" (Var "b"))
          (Conj 
            (Unify "b" "c")
            (Unify "a" (Value 7)))))))
  emptyState

-- [State{stSubsts =
--          fromList [(0, Value 7), (1, Var (Id "c")), (2, Value 7)],
--        stNextVar = 3,
--        stVarMap =
--          fromList [(Id "a", 0), (Id "b", 1), (Id "c", 2)]}]

main :: IO ()
main = putStrLn (groom tryingResult)

-- Length, number of lines.
-- MicroKanren: 39
-- This: 32
-- This with imports and declarations: 71
-- New (incl decls): 56
-- New (excl decl): 39

-- tryingResult1 = eval
--   (Conj 
--     (Let (\v -> Unify v (Value 7)))
--     (Let (\b -> Disj 
--       (Unify b (Value 5))
--       (Unify b (Value 6)))))
--   emptyState
