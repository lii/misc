{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE GADTs #-}

module Main where

data C where
  C :: (a -> a -> a) -> C
  D :: Int -> C

chooseStringWeirdRankN :: (forall a. a -> a -> a) -> String -> String -> String
chooseStringWeirdRankN choice s1 s2 =
  if choice True False then choice s1 s2 else choice s2 s1

chooseStringWeirdExistensial :: (forall a. a -> a -> a) -> String -> String -> String
chooseStringWeirdExistensial choice s1 s2 =
  if choice True False then choice s1 s2 else choice s2 s1


-- @Lqueryvg: foldl can actually also be stopped, if the argument function does not use its element argument. I posted an answer about this. – Lii Jul 19 at 13:40   

stopLeft :: Int
stopLeft = foldl
  (\_acc _e -> 7)
  0 ([1 :: Int, 2] ++ error "Evaluated!")

main :: IO ()
main = putStrLn ("Or: " ++ show stopLeft)

a :: Bool
a = and ([True, True, False] ++ repeat True)

o :: Bool
o = or ([False, False, True] ++ repeat False)

newAnd :: [Bool] -> Bool
newAnd l = foldr (&&) True l 
-- and l = foldl (\acc e -> if acc then e else False) True l 

newOr :: [Bool] -> Bool
newOr l = foldr (||) False l
-- or l = foldl (\acc e -> if acc then True else e) False l


